using UnityEngine;
using System.Collections;

public class LocalUIInput : KhetInput {
    static LocalUIInput currentinstance;
    public static bool IsGameOver = false;
    public static LocalUIInput CurrentInstance
    {
        get
        {
            if (currentinstance != null)
                return currentinstance;
            return null;
        }
    }
    public LocalUIInput(KhetColor color, string name)
    {
        this.color = color;
        this.name = name;
    }
    bool AcceptingInput = false;
    private KhetColor color;
    private GameObject highlighter;
    private GameObject selection;
    private Vector2 selpos;
    public void Update()
    {
        if (IsGameOver)
            return;
        if (AcceptingInput && !GameGUIManager.areyousuredialog)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                GameObject.Destroy(selection);
            if (selection)
                GameGUIManager.ShowObjectTools = true;
            else
                GameGUIManager.ShowObjectTools = false;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000);
            if (hit.collider != null)
            {
                int x = (int)Mathf.Round(hit.point.x / 10);
                int y = (int)Mathf.Round(hit.point.z / 10);
                if (Input.GetMouseButtonDown(0))
                {
                    //Debug.Log(hit.point);
                    if (GameArea.area[x, y].obj != null && (selection == null || GameArea.area[(int)selpos.x, (int)selpos.y].obj.type.name != "Scarab"))
                    {
                        Debug.Log(1);
                        if (selection != null)
                            GameObject.Destroy(selection);
                        if (GameArea.area[x, y].obj.color == color)
                        {
                            selpos = new Vector2(x, y);
                            selection = (GameObject)GameObject.Instantiate((Object)Resources.Load("Selection"), new Vector3(x * 10, 0.5f, y * 10), Quaternion.identity);
                        }
                    }
                    else
                    {
                        if (selection != null)
                        {
                            if (OnMove((int)selpos.x, (int)selpos.y, x, y))
                                GameObject.Destroy(selection);
                        }
                    }
                }
                if (highlighter == null)
                {
                    highlighter = (GameObject)GameObject.Instantiate((Object)Resources.Load("Highlighter"), new Vector3(x * 10, 0.5f, y * 10), Quaternion.identity);
                }
                highlighter.transform.position = new Vector3(x * 10, 0.05f, y * 10);
            }
            else
            {
                GameObject.Destroy(highlighter);
            }
        }
    }
    public override void GameOver(Result result)
    {
        if(color == KhetColor.Silver)
        {
            if (result == Result.SilverWon)
                GameGUIManager.GameResult = "Silver won!";
            else if (result == Result.RedWon)
                GameGUIManager.GameResult = "Red won!";
            else
                GameGUIManager.GameResult = "Draw!";
        }
        if (color == KhetColor.Red)
        {
            if (result == Result.SilverWon)
                GameGUIManager.GameResult = "Silver won!";
            else if (result == Result.RedWon)
                GameGUIManager.GameResult = "Red won!";
            else
                GameGUIManager.GameResult = "Draw!";
        }
    }
    public void RotateClockwise()
    {
        OnRotate((int)selpos.x, (int)selpos.y, true);
        GameObject.Destroy(selection);
    }
    public void RotateCounterClockwise()
    {
        OnRotate((int)selpos.x, (int)selpos.y, false);
        GameObject.Destroy(selection);
    }
    public override void StartInput()
    {
        AcceptingInput = true;
        currentinstance = this;
        GameGUIManager.RotateClockwise = RotateClockwise;
        GameGUIManager.RotateCounterClockwise = RotateCounterClockwise;
    }
    public override void StopInput()
    {
        Debug.Log("Test");
        AcceptingInput = false;
        GameGUIManager.ShowObjectTools = false;
        GameObject.Destroy(highlighter);
        GameObject.Destroy(selection);
    }
}
